import functools
import json
from datetime import datetime, timedelta
from typing import Any, Callable, Tuple, TypeVar, cast

import requests
from rich.console import Console

F = TypeVar("F", bound=Callable[..., Any])


def get_json(url: str, **kwargs: Any) -> Any:
    """Send GET request to target endpoint and retrieve JSON response.

    Parameters
    ----------
    url : str
        API endpoint url.
    **kwargs :
        optional keyword request parameters.

    Returns
    -------
    Any
        json-encoded content of a response, if any.
    """

    response = requests.get(url=url, params=kwargs)
    return response.json()


def jprint(obj: Any) -> None:
    """Print a formatted representation of the input Python JSON object.

    Parameters
    ----------
    obj : Any
        JSON object.
    """

    text = json.dumps(obj, sort_keys=True, indent=2)
    print(text)


def spinner(func: F) -> F:
    """Display a spinning progress icon during function execution."""

    message = "processing papers"
    spinner = "earth"

    @functools.wraps(func)
    def wrapper(*args, **kwargs):  # type: ignore

        with Console().status(message, spinner=spinner):

            for res in func(*args, **kwargs):
                yield res

    return cast(F, wrapper)


def calc_timerange(n: int) -> Tuple[str, str]:
    """Calculate time range corresponding to last n days."""

    today = datetime.today()
    beginning = today - timedelta(n)
    end = today.strftime("%Y-%m-%d")
    start = beginning.strftime("%Y-%m-%d")

    return start, end
