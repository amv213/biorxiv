import textwrap
from typing import List

import click

from biorxiv.client import Client
from biorxiv.filters import Filter
from biorxiv.resources.details import DetailsMostRecentDays

BANNER = r"""
    __    _       ____       _
   / /_  (_)___  / __ \_  __(_)   __
  / __ \/ / __ \/ /_/ / |/_/ / | / /
 / /_/ / / /_/ / _, _/>  </ /| |/ /
/_.___/_/\____/_/ |_/_/|_/_/ |___/

"""


@click.group()
def main() -> None:
    """The bioRxiv CLI."""

    click.echo(BANNER)

    pass


@main.command()
@click.option(
    "-d", "--days", default=7, help="number of days for which to get feed"
)
@click.option("-c", "--category", multiple=True, help="category selector")
@click.option(
    "-a",
    "--affiliation",
    multiple=True,
    help="author_corresponding_institution selector",
)
@click.option("-t", "--title", multiple=True, help="title selector")
def feed(
    days: int, category: List[str], affiliation: List[str], title: List[str]
) -> None:
    """Paper feed"""

    SELECTORS = {
        "category": category,
        "author_corresponding_institution": affiliation,
        "title": title,
    }

    resource = DetailsMostRecentDays(days=days)
    client = Client(resource)
    filter = Filter(SELECTORS)

    data = client.query()
    data = filter.apply(data)

    # consume iterator
    wrapper = textwrap.TextWrapper(
        width=119, initial_indent="   ", subsequent_indent="   "
    )
    for paper in data:
        click.secho(f"\n📑 {paper['title']}:", bold=True)
        click.secho(wrapper.fill(paper["authors"]), dim=True)
        click.secho(f"   DOI: {paper['doi']}\n", dim=True)
        click.echo(wrapper.fill(paper["abstract"]))
    click.echo("\n")
