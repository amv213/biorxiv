from typing import Generic, List, Literal, TypeVar

import pydantic
import pydantic.generics

ResponseStatus = Literal["ok", "no posts found"]
BASE_SCHEMA = pydantic.BaseModel


class Message(BASE_SCHEMA):
    """Schema of a simple BioRxiv message object."""

    status: ResponseStatus


class PaginatedMessage(Message):
    """Schema of a simple BioRxiv message for paginated data."""

    interval: str
    cursor: int
    count: int
    total: int


M = TypeVar("M", bound=Message)
C = TypeVar("C", bound=BASE_SCHEMA)


class GenericSchema(pydantic.generics.GenericModel, Generic[M, C]):
    """Schema of a generic BioRxiv endpoint response."""

    messages: List[M]
    collection: List[C]
