from typing import Generic, List

from . import BASE_SCHEMA, GenericSchema, M, Message, PaginatedMessage


class PaginatedDetailsMessage(PaginatedMessage):
    """Schema of a BioRxiv message for paginated papers."""

    count_new_papers: int


class Paper(BASE_SCHEMA):
    """Schema of a BioRxiv paper object."""

    doi: str
    title: str
    authors: str
    author_corresponding: str
    author_corresponding_institution: str
    date: str
    version: str
    type: str
    license: str
    category: str
    jatsxml: str
    abstract: str
    published: str
    server: str


class DetailsSchema(GenericSchema[M, Paper], Generic[M]):
    """Schema of a generic BioRxiv endpoint response."""

    collection: List[Paper]


class DetailsSchemaSingle(DetailsSchema[Message]):
    """Schema of a BioRxiv `/details` endpoint response for
    non-paginated request."""


class DetailsSchemaPaginated(DetailsSchema[PaginatedDetailsMessage]):
    """Schema of a BioRxiv `/details` endpoint response for range request."""
