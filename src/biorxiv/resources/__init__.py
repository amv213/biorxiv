"""BioRxiv API resources."""

import concurrent.futures as cf
import copy
import itertools
from abc import ABC, abstractmethod
from typing import Iterator, Type

from biorxiv import responses, utils


class Resource(ABC):
    """Abstract BioRxiv API resource."""

    # class variables
    root: str = "https://api.biorxiv.org"
    server: str = "biorxiv"

    @property
    @abstractmethod
    def response_type(self) -> Type[responses.Response]:
        """Specify resource response schema."""

    @abstractmethod
    def make_url(self) -> str:
        """Generate API request url."""

    @abstractmethod
    def paginate(self, step_size: int) -> "Resource":
        """Update object with updated pagination attributes."""

    def _get_response(self) -> responses.Response:
        """Send GET request to target endpoint and retrieve API response."""
        url = self.make_url()
        raw = utils.get_json(url=url)
        response = self.response_type(raw)
        return response

    def get_responses(self) -> Iterator[responses.Response]:
        """Send GET request to target endpoint and retrieve API responses."""

        # Get first page of responses
        response = self._get_response()

        if not response.ok():
            raise StopIteration

        # Yield if only one page
        if not response.more():
            yield response
            return

        # Build virtual resources targeting rest of pages
        page_size = response.count()
        available = response.total() - response.cursor()
        offsets = range(page_size, available + 1, page_size)
        resources = [
            copy.deepcopy(self).paginate(offset) for offset in offsets
        ]

        # get all new responses concurrently
        with cf.ThreadPoolExecutor() as executor:
            futures = [executor.submit(r._get_response) for r in resources]
            completed, _ = cf.wait(futures)  # wait for completion

        # chain all responses together in a single iterable
        responses = (job.result() for job in completed)
        all_responses = itertools.chain((response,), responses)

        yield from all_responses

    def get_data(self) -> Iterator[responses.JSONDict]:
        """Yield data from resource."""

        responses = self.get_responses()

        data = (response.get_data() for response in responses if response.ok())
        items = itertools.chain.from_iterable(data)

        yield from items
