"""BioRxiv /details enpoint resources."""

from dataclasses import dataclass
from typing import Type

from biorxiv.responses.details import (
    DetailsResponse,
    DetailsResponsePaginated,
    DetailsResponseSingle,
)

from .. import utils
from . import Resource


class Details(Resource):
    """Abstract BioRxiv /details endpoint resource."""

    # class variables
    endpoint: str = "details"

    response_type: Type[DetailsResponse]

    # most /details resources are not paginated
    def paginate(self, step_size: int) -> "Details":
        pass


@dataclass
class DetailsDOI(Details):
    """BioRxiv /details lookup given paper DOI."""

    doi: str

    response_type = DetailsResponseSingle

    def make_url(self) -> str:
        url = f"{self.root}/{self.endpoint}/{self.server}/{self.doi}"
        return url


@dataclass
class DetailsMostRecent(Details):
    """BioRxiv /details lookup for n most recent papers."""

    n: int

    response_type = DetailsResponseSingle

    def __post_init__(self) -> None:
        if self.n > 100:
            raise ValueError(
                "/details endpoint only returns n < 100 most recent entries."
            )

    def make_url(self) -> str:
        url = f"{self.root}/{self.endpoint}/{self.server}/{self.n}"
        return url


@dataclass
class DetailsRange(Details):
    """BioRxiv /details lookup for papers published in time window."""

    start: str
    end: str
    cursor: int

    response_type = DetailsResponsePaginated

    def make_url(self) -> str:
        interval = f"{self.start}/{self.end}"
        base = f"{self.root}/{self.endpoint}/{self.server}"
        url = f"{base}/{interval}/{self.cursor}"
        return url

    def paginate(self, step_size: int) -> "DetailsRange":
        self.cursor += step_size
        return self


class DetailsMostRecentDays(DetailsRange):
    """BioRxiv /details lookup for most recent x days of papers."""

    def __init__(self, days: int):
        self.days = days
        start, end = utils.calc_timerange(n=days)
        super().__init__(start=start, end=end, cursor=0)
