import pathlib
from typing import Iterator

import requests

from biorxiv.resources import Resource
from biorxiv.responses import JSONDict


class Client:
    def __init__(self, resource: Resource) -> None:

        self.resource = resource

    def query(self) -> Iterator[JSONDict]:
        """Query data from endpoint resource."""

        yield from self.resource.get_data()

    def download_paper(self, doi: str) -> None:
        """Download paper of given doi."""

        url = f"https://www.biorxiv.org/content/{doi}.full.pdf"

        try:
            response = requests.get(url, stream=True)
            response.raise_for_status()

        except requests.exceptions.HTTPError as err:
            print(err)
            return

        fdir = pathlib.Path("downloads")
        fdir.mkdir(parents=True, exist_ok=True)

        fn = f"{doi.split('/')[1]}.full.pdf"
        with open(fdir / fn, "wb") as f:
            f.write(response.content)
