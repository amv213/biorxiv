from typing import Type

from biorxiv.schemas import details

from . import GenericResponse, PaginatedResponseMixin, SingleResponseMixin


class DetailsResponse(GenericResponse):
    """Abstract BioRxiv /details endpoint response."""

    schema = Type[details.DetailsSchema]


class DetailsResponseSingle(DetailsResponse, SingleResponseMixin):
    """BioRxiv /details endpoint response for targeted request."""

    schema = details.DetailsSchemaSingle


class DetailsResponsePaginated(DetailsResponse, PaginatedResponseMixin):
    """BioRxiv /details endpoint response for range request."""

    schema = details.DetailsSchemaPaginated
