from abc import ABC, abstractmethod
from typing import Any, Dict, Iterator, List, Type, Union

from .. import schemas

JSONDict = Dict[str, Any]


class Response(ABC):
    """Abstract BioRxiv endpoint response."""

    @property
    @abstractmethod
    def schema(self) -> Type[schemas.BASE_SCHEMA]:
        """Response validation schema."""

    def __init__(self, raw: JSONDict):
        # validate input data
        self.parsed = self.schema(**raw).dict()

    @abstractmethod
    def get_metadata(self) -> List[JSONDict]:
        """Return response metadata."""

    @abstractmethod
    def get_data(self) -> Iterator[JSONDict]:
        """Return response payload."""

    @abstractmethod
    def get_status(self) -> Union[str, int, bool]:
        """Return response status."""

    @abstractmethod
    def ok(self) -> bool:
        """True if response status indicates success."""

    @abstractmethod
    def cursor(self) -> int:
        """Return cursor position"""

    @abstractmethod
    def total(self) -> int:
        """Return total number of available items."""

    @abstractmethod
    def count(self) -> int:
        """Return number of available items in current response."""

    def more(self) -> bool:
        """True if there are more items available than in current response."""
        return self.cursor() + self.count() < self.total()


class GenericResponse(Response):
    """Abstract generic BioRxiv endpoint response."""

    def get_metadata(self) -> List[JSONDict]:
        return self.parsed["messages"]

    def get_data(self) -> Iterator[JSONDict]:
        yield from self.parsed["collection"]

    def get_status(self) -> str:
        metadata = self.get_metadata()
        return metadata[0]["status"]

    def ok(self) -> bool:
        return self.get_status() == "ok"


class SingleResponseMixin(Response):
    """Abstract BioRxiv non-paginated endpoint response mixin."""

    def cursor(self) -> int:
        return 0

    def total(self) -> int:
        return len(list(self.get_data()))

    def count(self) -> int:
        return self.total()


class PaginatedResponseMixin(Response):
    """Abstract BioRxiv paginated endpoint response mixin."""

    def cursor(self) -> int:
        return self.get_metadata()[0]["cursor"]

    def total(self) -> int:
        return self.get_metadata()[0]["total"]

    def count(self) -> int:
        return self.get_metadata()[0]["count"]
