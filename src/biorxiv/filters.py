from dataclasses import dataclass
from typing import Dict, Iterator, List, Optional

from biorxiv.responses import JSONDict

Selectors = Dict[str, List[str]]


@dataclass
class Filter:

    selectors: Optional[Selectors] = None

    def criteria(self, obj: JSONDict) -> bool:
        """Pass if all object fields match selectors."""

        if not self.selectors:
            return False

        for field, value in obj.items():
            if field in self.selectors.keys():
                keywords = self.selectors[field]
                if keywords:
                    if any(kw in value for kw in keywords):
                        continue  # check next field
                    else:
                        return False  # discard obj
        return True

    def apply(self, objs: Iterator[JSONDict]) -> Iterator[JSONDict]:
        """Filter objects."""
        return filter(self.criteria, objs)
