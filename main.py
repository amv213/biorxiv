from typing import Any

from biorxiv.core import Client
from biorxiv.filters import Filter
from biorxiv.schemas import Paper


def process(paper: Paper) -> Any:
    """Sample processing function.

    Here just extracts fields from paper into a dictionary.
    """

    return paper.dict(include={"doi", "title", "abstract"})


SELECTORS = {
    "category": ["systems biology", "developmental biology"],
    "author_corresponding_institution": [
        "Philippines",
        "Vietnam",
    ],
}

if __name__ == "__main__":

    client = Client()
    filter = Filter(SELECTORS)

    # all_papers = client.fetch_papers(start="2021-02-05", end="2021-03-05")
    # all_papers = client.fetch_papers(days=7)  # get papers from last n days
    # filtered_papers = filter.apply(all_papers)
    # processed_papers = map(process, filtered_papers)

    # consume iterator
    # print("")
    # for paper in processed_papers:
    #    print(paper)

    print(client.fetch_paper(doi="10.1101/2021.02.05.429908"))
    # bioRxiv.download_paper(doi="10.1101/2021.02.05.429908")
